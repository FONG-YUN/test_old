<?php

namespace App\Http\Controllers;

use App\Thing;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index(){

        $things = Thing::all();
        foreach ($things as $thing){
            echo $thing->id;
            echo $thing->doThing . "<br>";
        }
        return view('frontend.todo');
    }

    public function add(Request $request){

        $thing = new Thing;
        $thing->dothing = $request->input("insert") ;
        $thing->save();
        echo "資料已新增成功";

    }

    public function delete(Request $request){

        $thing = Thing::find($request->input("delete"));
        $thing -> delete();
        echo "資料已刪除";

    }

    public function search(Request $request){

        $search = $request->input("search");
        $thing = Thing::where('doThing','LIKE','%'.$search.'%') -> get();
        foreach ($thing as $things){
            echo $things->id;
            echo $things->doThing . "<br>";
        }


    }
}
