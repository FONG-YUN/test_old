<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thing extends Model
{

    protected $table = 'Thing';  //指定資料表

    protected $fillable = [
        'doThing'  //欄位
    ];

}
