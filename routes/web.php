<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/abc', function () {
    return view('frontend.welcome', ['name' => 'meowww']);
});*/

Route::get('/test', 'TestController@index' ) ->name('get.index');
Route::post('test/show', 'TestController@add' ) ->name('post.show');
Route::post('test/delete', 'TestController@delete' ) ->name('post.delete');
Route::post('test/search', 'TestController@search' ) ->name('post.search');
