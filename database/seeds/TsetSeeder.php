<?php

use Illuminate\Database\Seeder;

class TsetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Test::create([
            'title' => '測試',
            'num' => 1,
        ]);
    }
}
